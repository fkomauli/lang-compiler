﻿using System;
using System.IO;
using Lang;

class Interpreter {

    static void Main(string[] args) {
        if (args.Length == 0) { PrintUsage(); return; }
        var filename = args[0];
        if (!File.Exists(filename)) { Console.WriteLine("Cannot find file " + filename); return; }
        var source = File.ReadAllText(filename);
        LangInterpreter.Execute(source);
    }

    static void PrintUsage() {
        Console.WriteLine("Usage: li <filename>");
    }
}
