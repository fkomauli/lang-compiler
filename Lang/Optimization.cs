﻿using System;
using System.Collections.Generic;

namespace Lang {

    public static class Optimizations {

        public static void Optimize(this Ast.Program program) {
            var statements = new List<Ast.Statement>();
            foreach (var statement in program.statements) {
                var optimized = statement.Optimize();
                if (optimized != null) { statements.Add(optimized); }
            }
            program.statements = statements;
        }

        public static Ast.Statement Optimize(this Ast.Statement statement) {
            if (statement is Ast.Instruction) { return (statement as Ast.Instruction).Optimize(); }
            return statement;
        }

        public static Ast.Instruction Optimize(this Ast.Instruction instruction) {
            if (instruction is Ast.Assignment) { return (instruction as Ast.Assignment).Optimize(); }
            if (instruction is Ast.If) { return (instruction as Ast.If).Optimize(); }
            if (instruction is Ast.Loop) { return (instruction as Ast.Loop).Optimize(); }
            if (instruction is Ast.ForLoop) { return (instruction as Ast.ForLoop).Optimize(); }
            if (instruction is Ast.ForEach) { return (instruction as Ast.ForEach).Optimize(); }
            if (instruction is Ast.Read) { return (instruction as Ast.Read).Optimize(); }
            if (instruction is Ast.Write) { return (instruction as Ast.Write).Optimize(); }
            if (instruction is Ast.Block) { return (instruction as Ast.Block).Optimize(); }
            if (instruction is Ast.EmptyStatement) { return (instruction as Ast.EmptyStatement).Optimize(); }
            return instruction;
        }

        public static Ast.Instruction Optimize(this Ast.Assignment assignment) {
            assignment.variable = assignment.variable.Optimize();
            assignment.expression = assignment.expression.Optimize();
            return assignment;
        }

        public static Ast.Instruction Optimize(this Ast.If selection) {
            if (selection is Ast.IfElse) { return (selection as Ast.IfElse).Optimize(); }
            selection.condition = selection.condition.Optimize();
            var ifBody = selection.ifBody.Optimize();
            if (ifBody == null) { return null; }
            if (selection.condition.IsAlwaysTrue()) { return ifBody; }
            if (selection.condition.IsAlwaysFalse()) { return null; }
            return selection;
        }

        public static Ast.Instruction Optimize(this Ast.IfElse selection) {
            selection.condition = selection.condition.Optimize();
            selection.ifBody = selection.ifBody.Optimize();
            selection.elseBody = selection.elseBody.Optimize();
            if (selection.ifBody == null && selection.elseBody == null) { return null; }
            if (selection.condition.IsAlwaysTrue()) { return selection.ifBody; }
            if (selection.condition.IsAlwaysFalse()) { return selection.elseBody; }
            if (selection.ifBody == null) { selection.ifBody = new Ast.EmptyStatement(); }
            if (selection.elseBody == null) { return new Ast.If { condition = selection.condition, ifBody = selection.ifBody }; }
            return selection;
        }

        public static Ast.Instruction Optimize(this Ast.Loop loop) {
            loop.condition = loop.condition.Optimize();
            loop.body = loop.body.Optimize();
            if (loop is Ast.While && loop.condition.IsAlwaysFalse()) { return null; }
            if (loop.body == null) { loop.body = new Ast.EmptyStatement(); }
            return loop;
        }

        public static Ast.Instruction Optimize(this Ast.ForLoop forLoop) {
            forLoop.variable = forLoop.variable.Optimize();
            forLoop.from = forLoop.from.Optimize();
            forLoop.to = forLoop.to.Optimize();
            forLoop.body = forLoop.body.Optimize();
            if (forLoop.body == null) { forLoop.body = new Ast.EmptyStatement(); }
            return forLoop;
        }

        public static Ast.Instruction Optimize(this Ast.ForEach forEach) {
            forEach.variable = forEach.variable.Optimize();
            forEach.body = forEach.body.Optimize();
            if (forEach.body == null) { forEach.body = new Ast.EmptyStatement(); }
            return forEach;
        }

        public static Ast.Instruction Optimize(this Ast.Read read) {
            read.variable = read.variable.Optimize();
            return read;
        }

        public static Ast.Instruction Optimize(this Ast.Write write) {
            write.expression = write.expression.Optimize();
            return write;
        }

        public static Ast.Instruction Optimize(this Ast.Block block) {
            var statements = new List<Ast.Statement>();
            foreach (var statement in block.statements) {
                var optimized = statement.Optimize();
                if (optimized != null) { statements.Add(optimized); }
            }
            if (statements.Count == 0) { return null; }
            block.statements = statements;
            return block;
        }

        public static Ast.Instruction Optimize(this Ast.EmptyStatement emptyStatement) {
            return null;
        }

        public static Ast.Expression Optimize(this Ast.Expression expression) {
            if (expression is Ast.Variable) { return (expression as Ast.Variable).Optimize(); }
            if (expression is Ast.BinaryOperator) { return (expression as Ast.BinaryOperator).Optimize(); }
            if (expression is Ast.UnaryOperator) { return (expression as Ast.UnaryOperator).Optimize(); }
            return expression;
        }

        public static Ast.Variable Optimize(this Ast.Variable variable) {
            if (variable is Ast.ArrayAccess) { return (variable as Ast.ArrayAccess).Optimize(); }
            return variable;
        }

        public static Ast.ArrayAccess Optimize(this Ast.ArrayAccess arrayAccess) {
            arrayAccess.index = arrayAccess.index.Optimize();
            return arrayAccess;
        }

        public static Ast.Expression Optimize(this Ast.BinaryOperator op) {
            op.leftOperand = op.leftOperand.Optimize();
            op.rightOperand = op.rightOperand.Optimize();
            if (op.leftOperand is Ast.Number && op.rightOperand is Ast.Number) {
                var value = op.Apply((op.leftOperand as Ast.Number).value, (op.rightOperand as Ast.Number).value);
                return new Ast.Number { value = value };
            }
            return op;
        }

        static int Apply(this Ast.BinaryOperator op, int left, int right) {
            if (op is Ast.Add) { return left + right; }
            if (op is Ast.Sub) { return left - right; }
            if (op is Ast.Mul) { return left * right; }
            if (op is Ast.Div) { return left / right; }
            if (op is Ast.Mod) { return left % right; }
            throw new ArgumentException("Unknown binary operator: " + op.GetType());
        }

        public static Ast.Expression Optimize(this Ast.UnaryOperator op) {
            op.operand = op.operand.Optimize();
            if (op is Ast.UnaryPlus) { return (op as Ast.UnaryPlus).Optimize(); }
            if (op is Ast.UnaryMinus) { return (op as Ast.UnaryMinus).Optimize(); }
            return op;
        }

        public static Ast.Expression Optimize(this Ast.UnaryPlus plus) {
            return plus.operand;
        }

        public static Ast.Expression Optimize(this Ast.UnaryMinus minus) {
            if (minus.operand is Ast.UnaryMinus) { return (minus.operand as Ast.UnaryMinus).operand; }
            if (minus.operand is Ast.Number) {
                var number = (minus.operand as Ast.Number);
                number.value = -number.value;
                return number;
            }
            return minus;
        }

        public static Ast.Condition Optimize(this Ast.Condition condition) {
            if (condition is Ast.Comparison) { return (condition as Ast.Comparison).Optimize(); }
            if (condition is Ast.AND) { return (condition as Ast.AND).Optimize(); }
            if (condition is Ast.OR) { return (condition as Ast.OR).Optimize(); }
            if (condition is Ast.NOT) { return (condition as Ast.NOT).Optimize(); }
            return condition;
        }

        public static Ast.Condition Optimize(this Ast.Comparison comparison) {
            comparison.left = comparison.left.Optimize();
            comparison.right = comparison.right.Optimize();
            return comparison;
        }

        public static Ast.Condition Optimize(this Ast.AND and) {
            and.left = and.left.Optimize();
            and.right = and.right.Optimize();
            if (and.left.IsAlwaysTrue()) { return and.right; }
            if (and.right.IsAlwaysTrue()) { return and.left; }
            return and;
        }

        public static Ast.Condition Optimize(this Ast.OR or) {
            or.left = or.left.Optimize();
            or.right = or.right.Optimize();
            if (or.left.IsAlwaysFalse()) { return or.right; }
            if (or.right.IsAlwaysFalse()) { return or.left; }
            return or;
        }

        public static Ast.Condition Optimize(this Ast.NOT not) {
            not.condition = not.condition.Optimize();
            if (not.condition is Ast.NOT) { return (not.condition as Ast.NOT).condition; }
            return not;
        }
    }

    public static class StaticEvaluation {

        public static bool IsAlwaysTrue(this Ast.Condition condition) {
            if (condition is Ast.Comparison) {
                var comparison = condition as Ast.Comparison;
                if (comparison.left is Ast.Number && comparison.right is Ast.Number) {
                    return comparison.Apply((comparison.left as Ast.Number).value, (comparison.right as Ast.Number).value);
                } else if (condition is Ast.EQ && comparison.left is Ast.Null && comparison.right is Ast.Null) {
                    return true;
                }
            }
            if (condition is Ast.AND) {
                var and = condition as Ast.AND;
                return and.left.IsAlwaysTrue() && and.right.IsAlwaysTrue();
            }
            if (condition is Ast.OR) {
                var or = condition as Ast.OR;
                return or.left.IsAlwaysTrue() || or.right.IsAlwaysTrue();
            }
            if (condition is Ast.NOT) { return (condition as Ast.NOT).IsAlwaysFalse(); }
            return false;
        }

        public static bool IsAlwaysFalse(this Ast.Condition condition) {
            if (condition is Ast.Comparison) {
                var comparison = condition as Ast.Comparison;
                if (comparison.left is Ast.Number && comparison.right is Ast.Number) {
                    return !comparison.Apply((comparison.left as Ast.Number).value, (comparison.right as Ast.Number).value);
                } else if (condition is Ast.NEQ && comparison.left is Ast.Null && comparison.right is Ast.Null) {
                    return true;
                }
            }
            if (condition is Ast.AND) {
                var and = condition as Ast.AND;
                return and.left.IsAlwaysFalse() || and.right.IsAlwaysFalse();
            }
            if (condition is Ast.OR) {
                var or = condition as Ast.OR;
                return or.left.IsAlwaysFalse() && or.right.IsAlwaysFalse();
            }
            if (condition is Ast.NOT) { return (condition as Ast.NOT).IsAlwaysTrue(); }
            return false;
        }

        static bool Apply(this Ast.Comparison op, int left, int right) {
            if (op is Ast.LT) { return left < right; }
            if (op is Ast.LEQ) { return left <= right; }
            if (op is Ast.GT) { return left > right; }
            if (op is Ast.GEQ) { return left >= right; }
            if (op is Ast.EQ) { return left == right; }
            if (op is Ast.NEQ) { return left != right; }
            throw new ArgumentException("Unknown boolean operator: " + op.GetType());
        }
    }
}
