﻿using Irony.Parsing;

namespace Lang {
    public struct Span {

        public uint fromLine;
        public uint toLine;
        public uint fromColumn;
        public uint toColumn;

        public static Span From(uint line) {
            return new Span { fromLine = line, toLine = line };
        }

        public static Span Of(ParseTreeNode node) {
            var location = node.Span.Location;
            return Span.From((uint)location.Line + 1, (uint)location.Column).To((uint)(location.Column + node.Span.Length));
        }

        public static Span From(uint line, uint column) {
            return new Span { fromLine = line, toLine = line, fromColumn = column, toColumn = column };
        }

        public Span To(uint column) {
            return new Span { fromLine = fromLine, toLine = toLine, fromColumn = fromColumn, toColumn = column };
        }

        public Span To(uint line, uint column) {
            return new Span { fromLine = fromLine, toLine = line, fromColumn = fromColumn, toColumn = column };
        }
    }
}
