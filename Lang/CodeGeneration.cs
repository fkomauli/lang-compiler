﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace Lang {

    public class LangCompiler {

        public static void Compile(string name, string source, bool optimize = false) {
            var ast = LangParser.Parse(source);
            if (ast == null) { return; }
            var context = ast.TypeCheck();
            if (context.Errors.Count != 0) {
                TypeError.Dump(context.Errors, source);
                return;
            }
            if (optimize) { ast.Optimize(); }
            var assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(name), AssemblyBuilderAccess.RunAndSave);
            var module = assembly.DefineDynamicModule(name, name + ".exe");
            var type = module.DefineType("Program", TypeAttributes.Public | TypeAttributes.Class);
            var main = type.DefineMethod("Main", MethodAttributes.Public | MethodAttributes.Static, typeof(void), new Type[] { typeof(string[]) });
            ast.Generate(new Code(main.GetILGenerator(), context.Symbols));
            type.CreateType();
            assembly.SetEntryPoint(main);
            assembly.Save(name + ".exe");
        }
    }

    public struct LoopLabels {
        public Label condition;
        public Label end;
    }

    public class Code {

        IDictionary<string, LocalBuilder> locals = new Dictionary<string, LocalBuilder>();
        ILGenerator generator;
        Stack<LoopLabels> loops = new Stack<LoopLabels>();

        public Code(ILGenerator generator, IDictionary<string, LangType> locals) {
            this.generator = generator;
            foreach (var entry in locals) { DeclareLocal(entry.Key, entry.Value); }
        }

        void DeclareLocal(string name, LangType type) {
            locals[name] = generator.DeclareLocal(type == LangType.INT ? typeof(int) : typeof(int[]));
        }

        public LocalBuilder GetLocal(Ast.Identifier id) {
            if (locals.ContainsKey(id.name)) { return locals[id.name]; }
            return null;
        }

        public bool IsDefined(Ast.Identifier id) { return locals.ContainsKey(id.name); }

        public ILGenerator Generator { get { return generator; } }

        public bool InLoop() { return loops.Count > 0; }

        public void EnterLoop(Label condition, Label end) {
            loops.Push(new LoopLabels { condition = condition, end = end });
        }

        public void ExitLoop() { loops.Pop(); }

        public Label InnermostLoopCondition() { return loops.Peek().condition; }

        public Label InnermostLoopEnd() { return loops.Peek().end; }
    }

    public static class CodeGeneration {

        public static void Generate(this Ast.Program program, Code code) {
            foreach (var statement in program.statements) { statement.Generate(code); }
            code.Generator.Emit(OpCodes.Ret);
        }

        public static void Generate(this Ast.Statement statement, Code code) {
            if (statement is Ast.Instruction) { (statement as Ast.Instruction).Generate(code); }
        }

        public static void Generate(this Ast.Instruction instruction, Code code) {
            if (instruction is Ast.Assignment) { (instruction as Ast.Assignment).Generate(code); return; }
            if (instruction is Ast.If) { (instruction as Ast.If).Generate(code); return; }
            if (instruction is Ast.While) { (instruction as Ast.While).Generate(code); return; }
            if (instruction is Ast.DoWhile) { (instruction as Ast.DoWhile).Generate(code); return; }
            if (instruction is Ast.ForLoop) { (instruction as Ast.ForLoop).Generate(code); return; }
            if (instruction is Ast.ForEach) { (instruction as Ast.ForEach).Generate(code); return; }
            if (instruction is Ast.Break) { (instruction as Ast.Break).Generate(code); return; }
            if (instruction is Ast.Continue) { (instruction as Ast.Continue).Generate(code); return; }
            if (instruction is Ast.Read) { (instruction as Ast.Read).Generate(code); return; }
            if (instruction is Ast.Write) { (instruction as Ast.Write).Generate(code); return; }
            if (instruction is Ast.Writemsg) { (instruction as Ast.Writemsg).Generate(code); return; }
            if (instruction is Ast.Writeln) { (instruction as Ast.Writeln).Generate(code); return; }
            if (instruction is Ast.Block) { (instruction as Ast.Block).Generate(code); return; }
            if (instruction is Ast.EmptyStatement) { (instruction as Ast.EmptyStatement).Generate(code); return; }
        }

        public static void Generate(this Ast.Assignment assignment, Code code) {
            assignment.variable.Assign(code, () => assignment.expression.Generate(code));
        }

        public static void Generate(this Ast.If selection, Code code) {
            var ifBodyEnd = code.Generator.DefineLabel();
            selection.condition.Generate(code);
            code.Generator.Emit(OpCodes.Brfalse, ifBodyEnd);
            selection.ifBody.Generate(code);
            if (selection is Ast.IfElse) {
                var elseBodyEnd = code.Generator.DefineLabel();
                code.Generator.Emit(OpCodes.Br, elseBodyEnd);
                code.Generator.MarkLabel(ifBodyEnd);
                (selection as Ast.IfElse).elseBody.Generate(code);
                code.Generator.MarkLabel(elseBodyEnd);
            } else {
                code.Generator.MarkLabel(ifBodyEnd);
            }
        }

        public static void Generate(this Ast.While loop, Code code) {
            var condition = code.Generator.DefineLabel();
            var end = code.Generator.DefineLabel();
            code.EnterLoop(condition, end);
            code.Generator.MarkLabel(condition);
            loop.condition.Generate(code);
            code.Generator.Emit(OpCodes.Brfalse, end);
            loop.body.Generate(code);
            code.Generator.Emit(OpCodes.Br, condition);
            code.Generator.MarkLabel(end);
            code.ExitLoop();
        }

        public static void Generate(this Ast.DoWhile loop, Code code) {
            var condition = code.Generator.DefineLabel();
            var end = code.Generator.DefineLabel();
            var body = code.Generator.DefineLabel();
            code.EnterLoop(condition, end);
            code.Generator.MarkLabel(body);
            loop.body.Generate(code);
            code.Generator.MarkLabel(condition);
            loop.condition.Generate(code);
            code.Generator.Emit(OpCodes.Brtrue, body);
            code.Generator.MarkLabel(end);
            code.ExitLoop();
        }

        public static void Generate(this Ast.ForLoop forLoop, Code code) {
            var condition = code.Generator.DefineLabel();
            var end = code.Generator.DefineLabel();
            var body = code.Generator.DefineLabel();
            code.EnterLoop(condition, end);
            forLoop.to.Generate(code);
            forLoop.variable.Assign(code, () => forLoop.from.Generate(code));
            code.Generator.Emit(OpCodes.Dup);
            forLoop.variable.Generate(code);
            code.Generator.Emit(OpCodes.Blt, end);
            code.Generator.MarkLabel(body);
            forLoop.body.Generate(code);
            code.Generator.MarkLabel(condition);
            code.Generator.Emit(OpCodes.Dup);
            forLoop.variable.Generate(code);
            code.Generator.Emit(OpCodes.Beq, end);
            forLoop.variable.Increment(code);
            code.Generator.Emit(OpCodes.Br, body);
            code.Generator.MarkLabel(end);
            code.Generator.Emit(OpCodes.Pop);
            code.ExitLoop();
        }

        public static void Generate(this Ast.ForEach forEach, Code code) {
            var variable = code.GetLocal(forEach.variable.identifier);
            var array = code.GetLocal(forEach.array);
            code.Generator.BeginScope();
            var index = code.Generator.DeclareLocal(typeof(int));
            var condition = code.Generator.DefineLabel();
            var end = code.Generator.DefineLabel();
            var body = code.Generator.DefineLabel();
            code.EnterLoop(condition, end);
            index.InitializeToZero(code);
            code.Generator.Emit(OpCodes.Br, condition);
            code.Generator.MarkLabel(body);
            forEach.variable.Assign(code, () => {
                code.Generator.Emit(OpCodes.Ldloc, array);
                code.Generator.Emit(OpCodes.Ldloc, index);
                code.Generator.Emit(OpCodes.Ldelem_I4);
            });
            index.Increment(code);
            forEach.body.Generate(code);
            code.Generator.MarkLabel(condition);
            code.Generator.Emit(OpCodes.Ldloc, index);
            ArrayLength(forEach.array, code);
            code.Generator.Emit(OpCodes.Blt, body);
            code.Generator.MarkLabel(end);
            code.ExitLoop();
            code.Generator.EndScope();
        }

        static void InitializeToZero(this LocalBuilder variable, Code code) {
            code.Generator.Emit(OpCodes.Ldc_I4_0);
            code.Generator.Emit(OpCodes.Stloc, variable);
        }

        static void Increment(this LocalBuilder variable, Code code) {
            code.Generator.Emit(OpCodes.Ldloc, variable);
            code.Generator.Emit(OpCodes.Ldc_I4_1);
            code.Generator.Emit(OpCodes.Add);
            code.Generator.Emit(OpCodes.Stloc, variable);
        }

        public static void Generate(this Ast.Break breakStatement, Code code) {
            if (code.InLoop()) { code.Generator.Emit(OpCodes.Br, code.InnermostLoopEnd()); }
        }

        public static void Generate(this Ast.Continue continueStatement, Code code) {
            if (code.InLoop()) { code.Generator.Emit(OpCodes.Br, code.InnermostLoopCondition()); }
        }

        static void ArrayLength(this Ast.Identifier id, Code code) {
            code.Generator.Emit(OpCodes.Ldloc, code.GetLocal(id));
            code.Generator.Emit(OpCodes.Ldlen);
        }

        static void Increment(this Ast.Variable variable, Code code) {
            variable.Assign(code, () => {
                variable.Generate(code);
                code.Generator.Emit(OpCodes.Ldc_I4_1);
                code.Generator.Emit(OpCodes.Add);
            });
        }

        public static void Generate(this Ast.Read read, Code code) {
            read.variable.Assign(code, () => {
                code.Generator.Emit(OpCodes.Call, typeof(Console).GetMethod("ReadLine"));
                code.Generator.Emit(OpCodes.Call, typeof(int).GetMethod("Parse", new Type[] { typeof(string) }));
            });
        }

        static void Assign(this Ast.Variable variable, Code code, Action evaluate) {
            if (variable is Ast.ArrayAccess) {
                code.Generator.Emit(OpCodes.Ldloc, code.GetLocal(variable.identifier));
                (variable as Ast.ArrayAccess).index.Generate(code);
            }
            evaluate();
            if (variable is Ast.ArrayAccess) {
                code.Generator.Emit(OpCodes.Stelem_I4);
            } else {
                code.Generator.Emit(OpCodes.Stloc, code.GetLocal(variable.identifier));
            }
        }

        public static void Generate(this Ast.Write write, Code code) {
            write.expression.Generate(code);
            code.Generator.Emit(OpCodes.Call, typeof(Console).GetMethod("Write", new Type[] { typeof(int) }));
        }

        public static void Generate(this Ast.Writemsg writemsg, Code code) {
            code.Generator.Emit(OpCodes.Ldstr, writemsg.message);
            code.Generator.Emit(OpCodes.Call, typeof(Console).GetMethod("Write", new Type[] { typeof(string) }));
        }

        public static void Generate(this Ast.Writeln writeln, Code code) {
            code.Generator.Emit(OpCodes.Call, typeof(Console).GetMethod("WriteLine", new Type[] { }));
        }

        public static void Generate(this Ast.Block block, Code code) {
            foreach (var statement in block.statements) { statement.Generate(code); }
        }

        public static void Generate(this Ast.EmptyStatement emptyStatement, Code code) { }

        public static void Generate(this Ast.Expression expression, Code code) {
            if (expression is Ast.Number) { (expression as Ast.Number).Generate(code); return; }
            if (expression is Ast.Variable) { (expression as Ast.Variable).Generate(code); return; }
            if (expression is Ast.Null) { (expression as Ast.Null).Generate(code); return; }
            if (expression is Ast.BinaryOperator) { (expression as Ast.BinaryOperator).Generate(code); return; }
            if (expression is Ast.UnaryOperator) { (expression as Ast.UnaryOperator).Generate(code); return; }
        }

        public static void Generate(this Ast.Number number, Code code) {
            code.Generator.Emit(OpCodes.Ldc_I4, number.value);
        }

        public static void Generate(this Ast.Variable variable, Code code) {
            if (variable is Ast.ArrayAccess) {
                (variable as Ast.ArrayAccess).Generate(code);
            } else {
                code.Generator.Emit(OpCodes.Ldloc, code.GetLocal(variable.identifier));
            }
        }

        public static void Generate(this Ast.ArrayAccess arrayAccess, Code code) {
            code.Generator.Emit(OpCodes.Ldloc, code.GetLocal(arrayAccess.identifier));
            arrayAccess.index.Generate(code);
            code.Generator.Emit(OpCodes.Ldelem_I4);
        }

        public static void Generate(this Ast.Null nullLiteral, Code code) {
            code.Generator.Emit(OpCodes.Ldnull);
        }

        public static void Generate(this Ast.BinaryOperator binaryOperator, Code code) {
            binaryOperator.leftOperand.Generate(code);
            binaryOperator.rightOperand.Generate(code);
            if (binaryOperator is Ast.Add) { code.Generator.Emit(OpCodes.Add); return; }
            if (binaryOperator is Ast.Sub) { code.Generator.Emit(OpCodes.Sub); return; }
            if (binaryOperator is Ast.Mul) { code.Generator.Emit(OpCodes.Mul); return; }
            if (binaryOperator is Ast.Div) { code.Generator.Emit(OpCodes.Div); return; }
            if (binaryOperator is Ast.Mod) { code.Generator.Emit(OpCodes.Rem); return; }
        }

        public static void Generate(this Ast.UnaryOperator unaryOperator, Code code) {
            unaryOperator.operand.Generate(code);
            if (unaryOperator is Ast.UnaryMinus) { code.Generator.Emit(OpCodes.Neg); return; }
            if (unaryOperator is Ast.NewArray) { code.Generator.Emit(OpCodes.Newarr, typeof(int)); return; }
            if (unaryOperator is Ast.ArrayLength) { code.Generator.Emit(OpCodes.Ldlen); return; }
        }

        public static void Generate(this Ast.Condition condition, Code code) {
            if (condition is Ast.Comparison) { (condition as Ast.Comparison).Generate(code); return; }
            if (condition is Ast.AND) { (condition as Ast.AND).Generate(code); return; }
            if (condition is Ast.OR) { (condition as Ast.OR).Generate(code); return; }
            if (condition is Ast.NOT) { (condition as Ast.NOT).Generate(code); return; }
        }

        public static void Generate(this Ast.Comparison condition, Code code) {
            condition.left.Generate(code);
            condition.right.Generate(code);
            if (condition is Ast.LT) { code.Generator.Emit(OpCodes.Clt); return; }
            if (condition is Ast.LEQ) { Leq(code); return; }
            if (condition is Ast.GT) { code.Generator.Emit(OpCodes.Cgt); return; }
            if (condition is Ast.GEQ) { Geq(code); return; }
            if (condition is Ast.EQ) { code.Generator.Emit(OpCodes.Ceq); return; }
            if (condition is Ast.NEQ) { Neq(code); return; }
        }

        public static void Generate(this Ast.AND and, Code code) {
            var shortCircuit = code.Generator.DefineLabel();
            and.left.Generate(code);
            code.Generator.Emit(OpCodes.Dup);
            code.Generator.Emit(OpCodes.Ldc_I4_0);
            code.Generator.Emit(OpCodes.Beq, shortCircuit);
            code.Generator.Emit(OpCodes.Pop);
            and.right.Generate(code);
            code.Generator.MarkLabel(shortCircuit);
        }

        public static void Generate(this Ast.OR or, Code code) {
            var shortCircuit = code.Generator.DefineLabel();
            or.left.Generate(code);
            code.Generator.Emit(OpCodes.Dup);
            code.Generator.Emit(OpCodes.Ldc_I4_1);
            code.Generator.Emit(OpCodes.Beq, shortCircuit);
            code.Generator.Emit(OpCodes.Pop);
            or.right.Generate(code);
            code.Generator.MarkLabel(shortCircuit);
        }

        public static void Generate(this Ast.NOT not, Code code) {
            not.condition.Generate(code);
            BoolNot(code);
        }

        static void BoolNot(Code code) {
            code.Generator.Emit(OpCodes.Ldc_I4_1);
            code.Generator.Emit(OpCodes.Xor);
        }

        static void Leq(Code code) {
            code.Generator.Emit(OpCodes.Cgt);
            BoolNot(code);
        }

        static void Geq(Code code) {
            code.Generator.Emit(OpCodes.Clt);
            BoolNot(code);
        }

        static void Neq(Code code) {
            code.Generator.Emit(OpCodes.Ceq);
            BoolNot(code);
        }
    }
}
