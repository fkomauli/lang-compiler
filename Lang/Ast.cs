﻿using System.Collections.Generic;

namespace Lang.Ast {

    public class Program {
        public List<Statement> statements;
    }

    public abstract class Node {
        public Span span;
    }

    public abstract class Statement : Node { }

    public abstract class Declaration : Statement {
        public List<Identifier> identifiers;
    }

    public class Var : Declaration { }

    public class Ref : Declaration { }

    public abstract class Instruction : Statement { }

    public class Assignment : Instruction {
        public Variable variable;
        public Expression expression;
    }

    public class If : Instruction {
        public Condition condition;
        public Instruction ifBody;
    }

    public class IfElse : If {
        public Instruction elseBody;
    }

    public abstract class Loop : Instruction {
        public Condition condition;
        public Instruction body;
    }

    public class While : Loop { }

    public class DoWhile : Loop { }

    public class ForLoop : Instruction {
        public Variable variable;
        public Expression from;
        public Expression to;
        public Instruction body;
    }

    public class ForEach : Instruction {
        public Variable variable;
        public Identifier array;
        public Instruction body;
    }

    public class Break : Instruction { }

    public class Continue : Instruction { }

    public class Read : Instruction {
        public Variable variable;
    }

    public class Write : Instruction {
        public Expression expression;
    }

    public class Writemsg : Instruction {
        public string message;
    }

    public class Writeln : Instruction { }

    public class Block : Instruction {
        public List<Statement> statements;
    }

    public class EmptyStatement : Instruction { }

    public abstract class Expression : Node { }

    public class Number : Expression {
        public int value;
    }

    public class Variable : Expression {
        public Identifier identifier;
    }

    public class ArrayAccess : Variable {
        public Expression index;
    }

    public class Null : Expression { }

    public abstract class BinaryOperator : Expression {
        public Expression leftOperand;
        public Expression rightOperand;
    }

    public class Add : BinaryOperator { }

    public class Sub : BinaryOperator { }

    public class Mul : BinaryOperator { }

    public class Div : BinaryOperator { }

    public class Mod : BinaryOperator { }

    public abstract class UnaryOperator : Expression {
        public Expression operand;
    }

    public class UnaryMinus : UnaryOperator { }

    public class UnaryPlus : UnaryOperator { }

    public class NewArray : UnaryOperator { }

    public class ArrayLength : UnaryOperator { }

    public abstract class Condition : Node { }

    public abstract class Comparison : Condition {
        public Expression left;
        public Expression right;
    }

    public class LT : Comparison { }

    public class LEQ : Comparison { }

    public class GT : Comparison { }

    public class GEQ : Comparison { }

    public class EQ : Comparison { }

    public class NEQ : Comparison { }

    public abstract class BooleanOperator : Condition {
        public Condition left;
        public Condition right;
    }

    public class AND : BooleanOperator { }

    public class OR : BooleanOperator { }

    public class NOT : Condition {
        public Condition condition;
    }

    public class Identifier : Node {
        public string name;
    }
}
