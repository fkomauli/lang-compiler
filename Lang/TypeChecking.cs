﻿using System;
using System.Collections.Generic;

namespace Lang {

    public enum LangType { INT, REF }

    public struct TypeError {

        public string message;
        public Span span;

        public void Dump(string line) {
            Console.WriteLine("Error at line {0} (column {1}): {2}\n", span.fromLine, span.fromColumn, message);
            Console.WriteLine(line);
            for (uint i = 0; i < span.fromColumn; i++) { Console.Write(' '); }
            for (uint i = span.fromColumn; i < span.toColumn; i++) { Console.Write('^'); }
            Console.WriteLine();
        }

        public static void Dump(IList<TypeError> errors, string source) {
            var lines = source.Split('\n');
            foreach (var error in errors) { error.Dump(lines[error.span.fromLine - 1]); }
            Console.WriteLine("{0} ERROR{1} FOUND", errors.Count, errors.Count == 1 ? "" : "S");
        }
    }

    public interface TypeCheckingResult {
        IDictionary<string, LangType> Symbols { get; }
        IList<TypeError> Errors { get; }
    }

    public class Context : TypeCheckingResult {

        IDictionary<string, LangType> symbols = new Dictionary<string, LangType>();
        IList<TypeError> errors = new List<TypeError>();

        public IDictionary<string, LangType> Symbols { get { return symbols; } }

        public IList<TypeError> Errors { get { return errors; } }

        public LangType? this[Ast.Identifier id] {
            get {
                if (symbols.ContainsKey(id.name)) { return symbols[id.name]; }
                return null;
            }
        }

        public void AddInt(Ast.Identifier id) { symbols[id.name] = LangType.INT; }

        public void AddRef(Ast.Identifier id) { symbols[id.name] = LangType.REF; }

        public bool IsDefined(Ast.Identifier id) { return symbols.ContainsKey(id.name); }

        public void TypeErrorIn(Ast.Statement statement, string message) {
            errors.Add(new TypeError { span = statement.span, message = message });
        }

        public void TypeErrorIn(Ast.Expression expression, string message) {
            errors.Add(new TypeError { span = expression.span, message = message });
        }

        public void TypeErrorIn(Ast.Condition condition, string message) {
            errors.Add(new TypeError { span = condition.span, message = message });
        }

        public void TypeErrorIn(Ast.Identifier identifier, string message) {
            errors.Add(new TypeError { span = identifier.span, message = message });
        }
    }

    public static class TypeChecking {

        public static TypeCheckingResult TypeCheck(this Ast.Program program) {
            var context = new Context();
            foreach (var statement in program.statements) { statement.TypeCheck(context); }
            return context;
        }

        public static void TypeCheck(this Ast.Statement statement, Context context) {
            if (statement is Ast.Declaration) {
                (statement as Ast.Declaration).TypeCheck(context);
            } else if (statement is Ast.Instruction) {
                (statement as Ast.Instruction).TypeCheck(context);
            } else {
                throw new ArgumentException("Unknown statement type: " + statement.GetType());
            }
        }

        public static void TypeCheck(this Ast.Declaration declaration, Context context) {
            declaration.identifiers.ForEach(variable => {
                if (context.IsDefined(variable)) {
                    context.TypeErrorIn(variable, "Variable " + variable.name + " already defined");
                } else if (declaration is Ast.Var) {
                    context.AddInt(variable);
                } else if (declaration is Ast.Ref) {
                    context.AddRef(variable);
                } else {
                    throw new ArgumentException("Unknown declaration type: " + declaration.GetType());
                }
            });
        }

        public static void TypeCheck(this Ast.Instruction instruction, Context context) {
            if (instruction is Ast.Assignment) {
                (instruction as Ast.Assignment).TypeCheck(context);
            } else if (instruction is Ast.If) {
                (instruction as Ast.If).TypeCheck(context);
            } else if (instruction is Ast.Loop) {
                (instruction as Ast.Loop).TypeCheck(context);
            } else if (instruction is Ast.ForLoop) {
                (instruction as Ast.ForLoop).TypeCheck(context);
            } else if (instruction is Ast.ForEach) {
                (instruction as Ast.ForEach).TypeCheck(context);
            } else if (instruction is Ast.Break) {
                (instruction as Ast.Break).TypeCheck(context);
            } else if (instruction is Ast.Continue) {
                (instruction as Ast.Continue).TypeCheck(context);
            } else if (instruction is Ast.Read) {
                (instruction as Ast.Read).TypeCheck(context);
            } else if (instruction is Ast.Write) {
                (instruction as Ast.Write).TypeCheck(context);
            } else if (instruction is Ast.Writemsg) {
                (instruction as Ast.Writemsg).TypeCheck(context);
            } else if (instruction is Ast.Writeln) {
                (instruction as Ast.Writeln).TypeCheck(context);
            } else if (instruction is Ast.Block) {
                (instruction as Ast.Block).TypeCheck(context);
            } else if (instruction is Ast.EmptyStatement) {
                (instruction as Ast.EmptyStatement).TypeCheck(context);
            } else {
                throw new ArgumentException("Unknown instruction: " + instruction.GetType());
            }
        }

        public static void TypeCheck(this Ast.Assignment assignment, Context context) {
            var variableType = assignment.variable.ExpressionType(context);
            var expressionType = assignment.expression.ExpressionType(context);
            if (variableType == LangType.INT && expressionType == LangType.REF) {
                context.TypeErrorIn(assignment, "Cannot assign value of array type to int variable");
            } else if (variableType == LangType.REF && expressionType == LangType.INT) {
                context.TypeErrorIn(assignment, "Cannot assign value of int type to array variable");
            }
        }

        public static void TypeCheck(this Ast.If selection, Context context) {
            selection.condition.TypeCheck(context);
            selection.ifBody.TypeCheck(context);
            if (selection is Ast.IfElse) {
                (selection as Ast.IfElse).elseBody.TypeCheck(context);
            }
        }

        public static void TypeCheck(this Ast.Loop loop, Context context) {
            loop.condition.TypeCheck(context);
            loop.body.TypeCheck(context);
        }

        public static void TypeCheck(this Ast.ForLoop forLoop, Context context) {
            if (forLoop.variable.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(forLoop.variable, "Cannot loop on a variable of array type");
            }
            if (forLoop.from.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(forLoop.from, "Cannot initialize for loop variable with an array value");
            }
            if (forLoop.to.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(forLoop.to, "Cannot use as loop end an array value");
            }
            forLoop.body.TypeCheck(context);
        }

        public static void TypeCheck(this Ast.ForEach forEach, Context context) {
            if (forEach.variable.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(forEach.variable, "Cannot assign array elements to a variable of array type in foreach loop");
            }
            if (forEach.array.GetTypeIn(context) == LangType.INT) {
                context.TypeErrorIn(forEach.array, "Cannot loop on an int variable, foreach requires an array variable");
            }
            forEach.body.TypeCheck(context);
        }

        public static void TypeCheck(this Ast.Break breakStatement, Context context) { }

        public static void TypeCheck(this Ast.Continue continueStatement, Context context) { }

        public static void TypeCheck(this Ast.Read read, Context context) {
            if (read.variable.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(read.variable, "Cannot store read value to array variable " + read.variable.identifier.name);
            }
        }

        public static void TypeCheck(this Ast.Write write, Context context) {
            if (write.expression.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(write.expression, "Cannot write an array expression");
            }
        }

        public static void TypeCheck(this Ast.Writemsg writemsg, Context context) { }

        public static void TypeCheck(this Ast.Writeln writeln, Context context) { }

        public static void TypeCheck(this Ast.Block block, Context context) {
            foreach (var statement in block.statements) { statement.TypeCheck(context); }
        }

        public static void TypeCheck(this Ast.EmptyStatement emptyStatement, Context context) { }

        public static LangType? ExpressionType(this Ast.Expression expression, Context context) {
            if (expression is Ast.Number) {
                return (expression as Ast.Number).ExpressionType(context);
            } else if (expression is Ast.Variable) {
                return (expression as Ast.Variable).ExpressionType(context);
            } else if (expression is Ast.Null) {
                return (expression as Ast.Null).ExpressionType(context);
            } else if (expression is Ast.BinaryOperator) {
                return (expression as Ast.BinaryOperator).ExpressionType(context);
            } else if (expression is Ast.UnaryOperator) {
                return (expression as Ast.UnaryOperator).ExpressionType(context);
            } else {
                throw new ArgumentException("Unknown expression type: " + expression.GetType());
            }
        }

        public static LangType? ExpressionType(this Ast.Number number, Context context) {
            return LangType.INT;
        }

        public static LangType? ExpressionType(this Ast.Variable variable, Context context) {
            if (variable is Ast.ArrayAccess) { return (variable as Ast.ArrayAccess).ExpressionType(context); }
            return variable.identifier.GetTypeIn(context);
        }

        public static LangType? ExpressionType(this Ast.ArrayAccess arrayAccess, Context context) {
            if (arrayAccess.identifier.GetTypeIn(context) == LangType.INT) {
                context.TypeErrorIn(arrayAccess, "Cannot access by index int variable " + arrayAccess.identifier.name);
            }
            if (arrayAccess.index.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(arrayAccess.index, "Array index must be an expression of type int");
            }
            return LangType.INT;
        }

        public static LangType? ExpressionType(this Ast.Null nullLiteral, Context context) {
            return LangType.REF;
        }

        public static LangType? ExpressionType(this Ast.BinaryOperator binaryOperator, Context context) {
            if (binaryOperator.leftOperand.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(binaryOperator.leftOperand, "Binary operator expects a left operand of type int");
            }
            if (binaryOperator.rightOperand.ExpressionType(context) == LangType.REF) {
                context.TypeErrorIn(binaryOperator.rightOperand, "Binary operator expects a right operand of type int");
            }
            return LangType.INT;
        }

        public static LangType? ExpressionType(this Ast.UnaryOperator unaryOperator, Context context) {
            var operandType = unaryOperator.operand.ExpressionType(context);
            if (unaryOperator is Ast.UnaryMinus && operandType == LangType.REF) {
                context.TypeErrorIn(unaryOperator.operand, "The unary minus operator expects an operand of type int");
            } else if (unaryOperator is Ast.UnaryPlus && operandType == LangType.REF) {
                context.TypeErrorIn(unaryOperator.operand, "The unary plus operator expects an operand of type int");
            } else if (unaryOperator is Ast.NewArray && operandType == LangType.REF) {
                context.TypeErrorIn(unaryOperator.operand, "The array creation operator expects an operand of type int");
            } else if (unaryOperator is Ast.ArrayLength && operandType == LangType.INT) {
                context.TypeErrorIn(unaryOperator.operand, "The array length operator expects an operand of type array");
            }
            if (unaryOperator is Ast.UnaryMinus || unaryOperator is Ast.UnaryPlus || unaryOperator is Ast.ArrayLength) { return LangType.INT; }
            if (unaryOperator is Ast.NewArray) { return LangType.REF; }
            return null;
        }

        public static void TypeCheck(this Ast.Condition condition, Context context) {
            if (condition is Ast.Comparison) { (condition as Ast.Comparison).TypeCheck(context); return; }
            if (condition is Ast.BooleanOperator) { (condition as Ast.BooleanOperator).TypeCheck(context); return; }
            if (condition is Ast.NOT) { (condition as Ast.NOT).TypeCheck(context); return; }
        }

        public static void TypeCheck(this Ast.Comparison comparison, Context context) {
            var leftType = comparison.left.ExpressionType(context);
            var rightType = comparison.right.ExpressionType(context);
            if (comparison is Ast.EQ || comparison is Ast.NEQ) {
                if (leftType == null || rightType == null) { return; }
                if (leftType != rightType) {
                    context.TypeErrorIn(comparison, "Cannot compare values of int and array type");
                }
            } else {
                if (leftType == LangType.REF) {
                    context.TypeErrorIn(comparison.left, "Cannot check ordering of values of array type");
                }
                if (rightType == LangType.REF) {
                    context.TypeErrorIn(comparison.left, "Cannot check ordering of values of array type");
                }
            }
        }

        public static void TypeCheck(this Ast.BooleanOperator op, Context context) {
            op.left.TypeCheck(context);
            op.right.TypeCheck(context);
        }

        public static void TypeCheck(this Ast.NOT not, Context context) {
            not.condition.TypeCheck(context);
        }

        public static LangType? GetTypeIn(this Ast.Identifier identifier, Context context) {
            if (!context.IsDefined(identifier)) {
                context.TypeErrorIn(identifier, "Variable " + identifier.name + " not defined");
            }
            return context[identifier];
        }
    }
}
