﻿using System;
using System.Collections.Generic;

namespace Lang {

    public class LangInterpreter {

        public static void Execute(string source) {
            var ast = LangParser.Parse(source);
            if (ast == null) { return; }
            var context = ast.TypeCheck();
            if (context.Errors.Count != 0) {
                TypeError.Dump(context.Errors, source);
                return;
            }
            ast.Evaluate(new Environment(context.Symbols));
        }
    }

    public struct Value {

        int[] value;
        LangType type;

        public static Value AsInt(int value) {
            return new Value { value = new int[] { value }, type = LangType.INT };
        }

        public static Value AsRef(int[] array) {
            return new Value { value = array, type = LangType.REF };
        }

        public int AsInt() { return value[0]; }

        public int[] AsRef() { return value; }

        public LangType Type() { return type; }
    }

    public class Environment {

        IDictionary<string, Value> variables = new Dictionary<string, Value>();
        uint nestedLoops = 0;

        public Environment(IDictionary<string, LangType> locals) {
            foreach (var entry in locals) {
                if (entry.Value == LangType.INT) { ReserveInt(entry.Key); }
                if (entry.Value == LangType.REF) { ReserveRef(entry.Key); }
            }
        }

        void ReserveInt(string name) { variables[name] = Value.AsInt(0); }

        void ReserveRef(string name) { variables[name] = Value.AsRef(null); }

        public Value this[Ast.Identifier id] {
            get { return variables[id.name]; }
        }

        public void Set(Ast.Identifier id, int value) {
            variables[id.name] = Value.AsInt(value);
        }

        public void Set(Ast.Identifier id, int[] array) {
            variables[id.name] = Value.AsRef(array);
        }

        public bool InLoop() { return nestedLoops > 0; }

        public void EnterLoop() { nestedLoops++; }

        public void ExitLoop() { nestedLoops--; }
    }

    public class BreakException : Exception { }

    public class ContinueException : Exception { }

    public static class Evaluation {

        public static void Evaluate(this Ast.Program program, Environment environment) {
            foreach (var statement in program.statements) { statement.Evaluate(environment); }
        }

        public static void Evaluate(this Ast.Statement statement, Environment environment) {
            if (statement is Ast.Instruction) { (statement as Ast.Instruction).Evaluate(environment); }
        }

        public static void Evaluate(this Ast.Instruction instruction, Environment environment) {
            if (instruction is Ast.Assignment) { (instruction as Ast.Assignment).Evaluate(environment); return; }
            if (instruction is Ast.If) { (instruction as Ast.If).Evaluate(environment); return; }
            if (instruction is Ast.While) { (instruction as Ast.While).Evaluate(environment); return; }
            if (instruction is Ast.DoWhile) { (instruction as Ast.DoWhile).Evaluate(environment); return; }
            if (instruction is Ast.ForLoop) { (instruction as Ast.ForLoop).Evaluate(environment); return; }
            if (instruction is Ast.ForEach) { (instruction as Ast.ForEach).Evaluate(environment); return; }
            if (instruction is Ast.Break) { (instruction as Ast.Break).Evaluate(environment); return; }
            if (instruction is Ast.Continue) { (instruction as Ast.Continue).Evaluate(environment); return; }
            if (instruction is Ast.Read) { (instruction as Ast.Read).Evaluate(environment); return; }
            if (instruction is Ast.Write) { (instruction as Ast.Write).Evaluate(environment); return; }
            if (instruction is Ast.Writemsg) { (instruction as Ast.Writemsg).Evaluate(environment); return; }
            if (instruction is Ast.Writeln) { (instruction as Ast.Writeln).Evaluate(environment); return; }
            if (instruction is Ast.Block) { (instruction as Ast.Block).Evaluate(environment); return; }
            if (instruction is Ast.EmptyStatement) { (instruction as Ast.EmptyStatement).Evaluate(environment); return; }
        }

        public static void Evaluate(this Ast.Assignment assignment, Environment environment) {
            var id = assignment.variable.identifier;
            if (assignment.variable is Ast.ArrayAccess) {
                var variable = assignment.variable as Ast.ArrayAccess;
                var array = environment[id].AsRef();
                var index = variable.index.Evaluate(environment).AsInt();
                var value = assignment.expression.Evaluate(environment).AsInt();
                array[index] = value;
            } else {
                var value = assignment.expression.Evaluate(environment);
                if (value.Type() == LangType.REF) {
                    environment.Set(id, value.AsRef());
                } else {
                    environment.Set(id, value.AsInt());
                }
            }
        }

        public static void Evaluate(this Ast.If selection, Environment environment) {
            if (selection.condition.Evaluate(environment)) {
                selection.ifBody.Evaluate(environment);
            } else if (selection is Ast.IfElse) {
                (selection as Ast.IfElse).elseBody.Evaluate(environment);
            }
        }

        public static void Evaluate(this Ast.While loop, Environment environment) {
            environment.EnterLoop();
            while (loop.condition.Evaluate(environment)) {
                try {
                    loop.body.Evaluate(environment);
                } catch (BreakException) {
                    break;
                } catch (ContinueException) {
                    continue;
                }
            }
            environment.ExitLoop();
        }

        public static void Evaluate(this Ast.DoWhile loop, Environment environment) {
            environment.EnterLoop();
            do {
                try {
                    loop.body.Evaluate(environment);
                } catch (BreakException) {
                    break;
                } catch (ContinueException) {
                    continue;
                }
            } while (loop.condition.Evaluate(environment));
            environment.ExitLoop();
        }

        public static void Evaluate(this Ast.ForLoop forLoop, Environment environment) {
            var id = forLoop.variable.identifier;
            var from = forLoop.from.Evaluate(environment).AsInt();
            var to = forLoop.to.Evaluate(environment).AsInt();
            if (forLoop.variable is Ast.ArrayAccess) {
                var variable = forLoop.variable as Ast.ArrayAccess;
                var array = environment[id].AsRef();
                var index = variable.index.Evaluate(environment).AsInt();
                environment.EnterLoop();
                for (int i = from; i <= to; i++) {
                    array[index] = i;
                    try {
                        forLoop.body.Evaluate(environment);
                    } catch (BreakException) {
                        break;
                    } catch (ContinueException) {
                        continue;
                    }
                }
                environment.ExitLoop();
            } else {
                environment.EnterLoop();
                for (int i = from; i <= to; i++) {
                    environment.Set(id, i);
                    try {
                        forLoop.body.Evaluate(environment);
                    } catch (BreakException) {
                        break;
                    } catch (ContinueException) {
                        continue;
                    }
                }
                environment.ExitLoop();
            }
        }

        public static void Evaluate(this Ast.ForEach forEach, Environment environment) {
            var id = forEach.variable.identifier;
            var iterable = environment[forEach.array].AsRef();
            if (forEach.variable is Ast.ArrayAccess) {
                var variable = forEach.variable as Ast.ArrayAccess;
                var array = environment[id].AsRef();
                var index = variable.index.Evaluate(environment).AsInt();
                environment.EnterLoop();
                for (int i = 0; i < iterable.Length; i++) {
                    array[index] = iterable[i];
                    try {
                        forEach.body.Evaluate(environment);
                    } catch (BreakException) {
                        break;
                    } catch (ContinueException) {
                        continue;
                    }
                }
                environment.ExitLoop();
            } else {
                environment.EnterLoop();
                for (int i = 0; i < iterable.Length; i++) {
                    environment.Set(id, iterable[i]);
                    try {
                        forEach.body.Evaluate(environment);
                    } catch (BreakException) {
                        break;
                    } catch (ContinueException) {
                        continue;
                    }
                }
                environment.ExitLoop();
            }
        }

        public static void Evaluate(this Ast.Break breakStatement, Environment environment) {
            if (environment.InLoop()) { throw new BreakException(); }
        }

        public static void Evaluate(this Ast.Continue continueStatement, Environment environment) {
            if (environment.InLoop()) { throw new ContinueException(); }
        }

        public static void Evaluate(this Ast.Read read, Environment environment) {
            var value = int.Parse(Console.ReadLine());
            if (read.variable is Ast.ArrayAccess) {
                var variable = read.variable as Ast.ArrayAccess;
                var array = environment[variable.identifier].AsRef();
                var index = variable.index.Evaluate(environment).AsInt();
                array[index] = value;
            } else {
                environment.Set(read.variable.identifier, value);
            }
        }

        public static void Evaluate(this Ast.Write write, Environment environment) {
            Console.Write(write.expression.Evaluate(environment).AsInt());
        }

        public static void Evaluate(this Ast.Writemsg writemsg, Environment environment) {
            Console.Write(writemsg.message);
        }

        public static void Evaluate(this Ast.Writeln writeln, Environment environment) {
            Console.WriteLine();
        }

        public static void Evaluate(this Ast.Block block, Environment environment) {
            foreach (var statement in block.statements) { statement.Evaluate(environment); }
        }

        public static void Evaluate(this Ast.EmptyStatement emptyStatement, Environment environment) { }

        public static Value Evaluate(this Ast.Expression expression, Environment environment) {
            if (expression is Ast.Number) { return (expression as Ast.Number).Evaluate(environment); }
            if (expression is Ast.Variable) { return (expression as Ast.Variable).Evaluate(environment); }
            if (expression is Ast.Null) { return (expression as Ast.Null).Evaluate(environment); }
            if (expression is Ast.BinaryOperator) { return (expression as Ast.BinaryOperator).Evaluate(environment); }
            if (expression is Ast.UnaryOperator) { return (expression as Ast.UnaryOperator).Evaluate(environment); }
            throw new ArgumentException("Unknown expression type: " + expression.GetType());
        }

        public static Value Evaluate(this Ast.Number number, Environment environment) {
            return Value.AsInt(number.value);
        }

        public static Value Evaluate(this Ast.Variable variable, Environment environment) {
            if (variable is Ast.ArrayAccess) {
                return (variable as Ast.ArrayAccess).Evaluate(environment);
            }
            return environment[variable.identifier];
        }

        public static Value Evaluate(this Ast.ArrayAccess arrayAccess, Environment environment) {
            var array = environment[arrayAccess.identifier].AsRef();
            var index = arrayAccess.index.Evaluate(environment).AsInt();
            return Value.AsInt(array[index]);
        }

        public static Value Evaluate(this Ast.Null nullReference, Environment environment) {
            return Value.AsRef(null);
        }

        public static Value Evaluate(this Ast.BinaryOperator binaryOperator, Environment environment) {
            var left = binaryOperator.leftOperand.Evaluate(environment).AsInt();
            var right = binaryOperator.rightOperand.Evaluate(environment).AsInt();
            return Value.AsInt(binaryOperator.Apply(left, right));
        }

        static int Apply(this Ast.BinaryOperator op, int left, int right) {
            if (op is Ast.Add) { return left + right; }
            if (op is Ast.Sub) { return left - right; }
            if (op is Ast.Mul) { return left * right; }
            if (op is Ast.Div) { return left / right; }
            if (op is Ast.Mod) { return left % right; }
            throw new ArgumentException("Unknown binary operator: " + op.GetType());
        }

        public static Value Evaluate(this Ast.UnaryOperator unaryOperator, Environment environment) {
            var value = unaryOperator.operand.Evaluate(environment);
            if (unaryOperator is Ast.UnaryMinus) { return Value.AsInt(-value.AsInt()); }
            if (unaryOperator is Ast.UnaryPlus) { return value; }
            if (unaryOperator is Ast.ArrayLength) { return Value.AsInt(value.AsRef().Length); }
            if (unaryOperator is Ast.NewArray) { return Value.AsRef(new int[value.AsInt()]); }
            throw new ArgumentException("Unknown unary operator: " + unaryOperator);
        }

        public static bool Evaluate(this Ast.Condition condition, Environment environment) {
            if (condition is Ast.Comparison) { return (condition as Ast.Comparison).Evaluate(environment); }
            if (condition is Ast.BooleanOperator) { return (condition as Ast.BooleanOperator).Evaluate(environment); }
            if (condition is Ast.NOT) { return (condition as Ast.NOT).Evaluate(environment); }
            throw new ArgumentException("Unknown conditional operator: " + condition.GetType());
        }

        public static bool Evaluate(this Ast.Comparison comparison, Environment environment) {
            var left = comparison.left.Evaluate(environment);
            var right = comparison.right.Evaluate(environment);
            if (left.Type() == LangType.REF && right.Type() == LangType.REF) {
                if (comparison is Ast.EQ) { return left.AsRef() == right.AsRef(); }
                if (comparison is Ast.NEQ) { return left.AsRef() != right.AsRef(); }
            } else {
                if (comparison is Ast.LT) { return left.AsInt() < right.AsInt(); }
                if (comparison is Ast.LEQ) { return left.AsInt() <= right.AsInt(); }
                if (comparison is Ast.GT) { return left.AsInt() > right.AsInt(); }
                if (comparison is Ast.GEQ) { return left.AsInt() >= right.AsInt(); }
                if (comparison is Ast.EQ) { return left.AsInt() == right.AsInt(); }
                if (comparison is Ast.NEQ) { return left.AsInt() != right.AsInt(); }
            }
            throw new ArgumentException("Unknown comparison operator: " + comparison.GetType());
        }

        public static bool Evaluate(this Ast.BooleanOperator op, Environment environment) {
            if (op is Ast.AND) { return op.left.Evaluate(environment) && op.right.Evaluate(environment); }
            if (op is Ast.OR) { return op.left.Evaluate(environment) || op.right.Evaluate(environment); }
            throw new ArgumentException("Unknown boolean operator: " + op.GetType());
        }

        public static bool Evaluate(this Ast.NOT not, Environment environment) {
            return !not.condition.Evaluate(environment);
        }
    }
}
