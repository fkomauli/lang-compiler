﻿using System;
using System.Collections.Generic;
using Irony.Ast;
using Irony.Parsing;
using Lang.Ast;

namespace Lang {

    public class LangParser {

        public static Program Parse(string source) {
            var grammar = new LangGrammar();
            var parser = new Parser(grammar);
            var parseTree = parser.Parse(source);
            if (parseTree.HasErrors()) {
                Dump(parseTree.ParserMessages, source);
                return null;
            }
            return parseTree.Root.AstNode as Program;
        }
        static void Dump(Irony.LogMessage log, string line) {
            Console.WriteLine("Error at line {0} (column {1}): {2}\n", log.Location.Line + 1, log.Location.Column, log.Message);
            Console.WriteLine(line);
            for (int i = 0; i < log.Location.Column; i++) { Console.Write(' '); }
            Console.WriteLine('^');
        }

        public static void Dump(Irony.LogMessageList logs, string source) {
            var lines = source.Split('\n');
            foreach (var log in logs) { Dump(log, lines[log.Location.Line]); }
            Console.WriteLine("{0} ERROR{1} FOUND", logs.Count, logs.Count == 1 ? "" : "S");
        }
    }

    public class LangGrammar : Grammar {

        static AstNodeCreator TakeAstNodeFromChild = (context, node) => { node.AstNode = node.ChildNodes[0].AstNode; };
        static AstNodeCreator DoNothing = (context, node) => { };

        public LangGrammar() : base(caseSensitive: true) {
            var Program = new NonTerminal("Program", Parse.Program);
            var DeclarationOrInstructionSequence = new NonTerminal("DeclarationOrInstructionSequence", Parse.Statements);
            var Declaration = new NonTerminal("Declaration", Parse.Declaration);
            var IdentifierSequence = new NonTerminal("IdentifierSequence", DoNothing);
            var Instruction = new NonTerminal("Instruction", TakeAstNodeFromChild);
            var Assignment = new NonTerminal("Assignment", Parse.Assignment);
            var Selection = new NonTerminal("Selection", Parse.Selection);
            var While = new NonTerminal("While", Parse.While);
            var DoWhile = new NonTerminal("DoWhile", Parse.DoWhile);
            var ForLoop = new NonTerminal("ForLoop", Parse.ForLoop);
            var ForEach = new NonTerminal("ForEach", Parse.ForEach);
            var Break = new NonTerminal("Break", Parse.Break);
            var Continue = new NonTerminal("Continue", Parse.Continue);
            var Read = new NonTerminal("Read", Parse.Read);
            var Write = new NonTerminal("Write", Parse.Write);
            var Block = new NonTerminal("Block", Parse.Block);
            var EmptyStatement = new NonTerminal("EmptyStatement", Parse.EmptyStatement);
            var Expression = new NonTerminal("Expression", Parse.Expression);
            var Condition = new NonTerminal("Condition", Parse.Condition);
            var Variable = new NonTerminal("Variable", Parse.Variable);
            var identifier = new IdentifierTerminal("identifier", "");
            identifier.AstConfig.NodeCreator = Parse.Identifier;
            var numberLiteral = new NumberLiteral("number", NumberOptions.IntOnly, DoNothing);
            var stringLiteral = new StringLiteral("string", "\"", StringOptions.None, DoNothing);

            var VAR = ToTerm("var");
            var REF = ToTerm("ref");
            var IF = ToTerm("if");
            var ELSE = ToTerm("else");
            var WHILE = ToTerm("while");
            var DO = ToTerm("do");
            var FOR = ToTerm("for");
            var BREAK = ToTerm("break");
            var CONTINUE = ToTerm("continue");
            var READ = ToTerm("read");
            var WRITE = ToTerm("write");
            var WRITEMSG = ToTerm("writemsg");
            var WRITELN = ToTerm("writeln");
            var NULL = ToTerm("null");
            var NEW = ToTerm("new");

            var INLINE_COMMENT = new CommentTerminal("inlineComment", "//", "\n", "\n\r");
            var BLOCK_COMMENT = new CommentTerminal("blockComment", "/*", "*/");

            Program.Rule
                = DeclarationOrInstructionSequence;
            DeclarationOrInstructionSequence.Rule
                = Empty
                | (DeclarationOrInstructionSequence + Declaration)
                | (DeclarationOrInstructionSequence + Instruction);
            Declaration.Rule
                = VAR + IdentifierSequence + ";"
                | REF + IdentifierSequence + ";";
            IdentifierSequence.Rule
                = identifier
                | IdentifierSequence + "," + identifier;
            Instruction.Rule
                = Assignment
                | Selection
                | While
                | DoWhile
                | ForLoop
                | ForEach
                | Break
                | Continue
                | Read
                | Write
                | Block
                | EmptyStatement;
            Assignment.Rule
                = Variable + "=" + Expression + ";";
            Selection.Rule
                = IF + "(" + Condition + ")" + Instruction
                | IF + "(" + Condition + ")" + Instruction + PreferShiftHere() + ELSE + Instruction;
            While.Rule
                = WHILE + "(" + Condition + ")" + Instruction;
            DoWhile.Rule
                = DO + Instruction + WHILE + "(" + Condition + ")";
            ForLoop.Rule
                = FOR + "(" + Variable + "=" + Expression + "," + Expression + ")" + Instruction;
            ForEach.Rule
                = FOR + "(" + Variable + ":" + identifier + ")" + Instruction;
            Break.Rule
                = BREAK + ";";
            Continue.Rule
                = CONTINUE + ";";
            Read.Rule
                = READ + Variable + ";";
            Write.Rule
                = WRITE + Expression + ";"
                | WRITEMSG + stringLiteral + ";"
                | WRITELN + ";";
            Block.Rule
                = "{" + DeclarationOrInstructionSequence + "}";
            EmptyStatement.Rule
                = ToTerm(";");
            Expression.Rule
                = numberLiteral
                | Variable
                | NULL
                | Expression + "+" + Expression
                | Expression + "-" + Expression
                | Expression + "*" + Expression
                | Expression + "/" + Expression
                | Expression + "%" + Expression
                | ImplyPrecedenceHere(10) + "-" + Expression
                | ImplyPrecedenceHere(10) + "+" + Expression
                | "(" + Expression + ")"
                | NEW + Expression
                | "#" + Expression;
            Condition.Rule
                = Expression + "<" + Expression
                | Expression + "<=" + Expression
                | Expression + ">" + Expression
                | Expression + ">=" + Expression
                | Expression + "==" + Expression
                | Expression + "!=" + Expression
                | Condition + "&&" + Condition
                | Condition + "||" + Condition
                | "!" + Condition
                | "(" + Condition + ")";
            Variable.Rule
                = identifier
                | identifier + "[" + Expression + "]";

            NonGrammarTerminals.Add(INLINE_COMMENT);
            NonGrammarTerminals.Add(BLOCK_COMMENT);

            Root = Program;

            MarkReservedWords(
                VAR.Text,
                REF.Text,
                IF.Text,
                ELSE.Text,
                WHILE.Text,
                DO.Text,
                FOR.Text,
                BREAK.Text,
                CONTINUE.Text,
                READ.Text,
                WRITE.Text,
                WRITEMSG.Text,
                WRITELN.Text,
                NULL.Text,
                NEW.Text
            );

            RegisterOperators(1, Associativity.Right, "=");
            RegisterOperators(2, Associativity.Left, "||");
            RegisterOperators(3, Associativity.Left, "&&");
            RegisterOperators(4, Associativity.Neutral, "==", "!=", "<", "<=", ">", ">=");
            RegisterOperators(5, Associativity.Neutral, "new");
            RegisterOperators(6, Associativity.Left, "+", "-");
            RegisterOperators(7, Associativity.Left, "*", "/", "%");
            RegisterOperators(8, Associativity.Neutral, "#");

            RegisterBracePair("(", ")");
            RegisterBracePair("[", "]");
            RegisterBracePair("{", "}");
            MarkPunctuation("(", ")", "[", "]", "{", "}", ";", ",", "=", ":");

            LanguageFlags = LanguageFlags.CreateAst;
        }
    }

    public static class Parse {

        public static void Program(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new Program { statements = parseNode.ChildNodes[0].AstNode as List<Statement> };
        }

        public static void Declaration(AstContext context, ParseTreeNode parseNode) {
            var identifiers = CollectIdentifiers(parseNode.ChildNodes[1]);
            switch (parseNode.ChildNodes[0].Token.Text) {
                case "var":
                    parseNode.AstNode = new Var { identifiers = identifiers, span = Span.Of(parseNode) };
                    break;
                case "ref":
                    parseNode.AstNode = new Ref { identifiers = identifiers, span = Span.Of(parseNode) };
                    break;
            }
        }

        static List<Identifier> CollectIdentifiers(ParseTreeNode node) {
            if (node.ChildNodes.Count == 1) {
                return new List<Identifier> { node.ChildNodes[0].AstNode as Identifier };
            } else {
                var identifiers = CollectIdentifiers(node.ChildNodes[0]);
                identifiers.Add(node.ChildNodes[1].AstNode as Identifier);
                return identifiers;
            }
        }

        public static void Assignment(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new Assignment {
                variable = parseNode.ChildNodes[0].AstNode as Variable,
                expression = parseNode.ChildNodes[1].AstNode as Expression,
                span = Span.Of(parseNode)
            };
        }

        public static void Selection(AstContext context, ParseTreeNode parseNode) {
            var condition = parseNode.ChildNodes[1].AstNode as Condition;
            var ifBody = parseNode.ChildNodes[2].AstNode as Instruction;
            if (parseNode.ChildNodes.Count == 5) {
                var elseBody = parseNode.ChildNodes[4].AstNode as Instruction;
                parseNode.AstNode = new IfElse { condition = condition, ifBody = ifBody, elseBody = elseBody, span = Span.Of(parseNode) };
            } else {
                parseNode.AstNode = new If { condition = condition, ifBody = ifBody, span = Span.Of(parseNode) };
            }
        }

        public static void While(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new While {
                condition = parseNode.ChildNodes[1].AstNode as Condition,
                body = parseNode.ChildNodes[2].AstNode as Instruction,
                span = Span.Of(parseNode)
            };
        }

        public static void DoWhile(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new DoWhile {
                body = parseNode.ChildNodes[1].AstNode as Instruction,
                condition = parseNode.ChildNodes[3].AstNode as Condition,
                span = Span.Of(parseNode)
            };
        }

        public static void ForLoop(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new ForLoop {
                variable = parseNode.ChildNodes[1].AstNode as Variable,
                from = parseNode.ChildNodes[2].AstNode as Expression,
                to = parseNode.ChildNodes[3].AstNode as Expression,
                body = parseNode.ChildNodes[4].AstNode as Instruction,
                span = Span.Of(parseNode)
            };
        }

        public static void ForEach(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new ForEach {
                variable = parseNode.ChildNodes[1].AstNode as Variable,
                array = parseNode.ChildNodes[2].AstNode as Identifier,
                body = parseNode.ChildNodes[3].AstNode as Instruction,
                span = Span.Of(parseNode)
            };
        }

        public static void Break(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new Break { span = Span.Of(parseNode) };
        }

        public static void Continue(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new Continue { span = Span.Of(parseNode) };
        }

        public static void Read(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new Read { variable = parseNode.ChildNodes[1].AstNode as Variable, span = Span.Of(parseNode) };
        }

        public static void Write(AstContext context, ParseTreeNode parseNode) {
            switch (parseNode.ChildNodes[0].Token.Text) {
                case "write":
                    parseNode.AstNode = new Write { expression = parseNode.ChildNodes[1].AstNode as Expression, span = Span.Of(parseNode) };
                    break;
                case "writemsg":
                    parseNode.AstNode = new Writemsg { message = parseNode.ChildNodes[1].Token.Value as string, span = Span.Of(parseNode) };
                    break;
                case "writeln":
                    parseNode.AstNode = new Writeln { span = Span.Of(parseNode) };
                    break;
            }
        }

        public static void Block(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new Block { statements = parseNode.ChildNodes[0].AstNode as List<Statement>, span = Span.Of(parseNode) };
        }

        public static void Statements(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = CollectStatements(parseNode);
        }

        static List<Statement> CollectStatements(ParseTreeNode node) {
            if (node.ChildNodes.Count == 0) {
                return new List<Statement>();
            } else {
                var statements = CollectStatements(node.ChildNodes[0]);
                statements.Add(node.ChildNodes[1].AstNode as Statement);
                return statements;
            }
        }

        public static void EmptyStatement(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new EmptyStatement { span = Span.Of(parseNode) };
        }

        public static void Expression(AstContext context, ParseTreeNode parseNode) {
            var childType = parseNode.ChildNodes[0].Term.Name;
            if (childType == "number") {
                var number = parseNode.ChildNodes[0].Token.Text;
                int value;
                if (!int.TryParse(number, out value)) {
                    context.AddMessage(Irony.ErrorLevel.Error, parseNode.ChildNodes[0].Span.Location, "Number must be a 32 bit integer: " + number);
                }
                parseNode.AstNode = new Number { value = value };
            } else if (childType == "Variable") {
                parseNode.AstNode = parseNode.ChildNodes[0].AstNode;
            } else if (childType == "null") {
                parseNode.AstNode = new Null();
            } else if (parseNode.ChildNodes.Count == 3) {
                var op = MatchBinaryOperator(parseNode.ChildNodes[1].Token.Text);
                op.leftOperand = parseNode.ChildNodes[0].AstNode as Expression;
                op.rightOperand = parseNode.ChildNodes[2].AstNode as Expression;
                parseNode.AstNode = op;
            } else if (parseNode.ChildNodes.Count == 2) {
                var op = MatchUnaryOperator(parseNode.ChildNodes[0].Token.Text);
                op.operand = parseNode.ChildNodes[1].AstNode as Expression;
                parseNode.AstNode = op;
            } else {
                parseNode.AstNode = parseNode.ChildNodes[0].AstNode;
            }
            (parseNode.AstNode as Expression).span = Span.Of(parseNode);
        }

        static BinaryOperator MatchBinaryOperator(string op) {
            switch (op) {
                case "+": return new Add();
                case "-": return new Sub();
                case "*": return new Mul();
                case "/": return new Div();
                case "%": return new Mod();
                default: throw new ArgumentException("Unknown binary operator: " + op);
            }
        }

        static UnaryOperator MatchUnaryOperator(string op) {
            switch (op) {
                case "-": return new UnaryMinus();
                case "+": return new UnaryPlus();
                case "new": return new NewArray();
                case "#": return new ArrayLength();
                default: throw new ArgumentException("Unknown unary operator: " + op);
            }
        }

        public static void Variable(AstContext context, ParseTreeNode parseNode) {
            var id = parseNode.ChildNodes[0].AstNode as Identifier;
            if (parseNode.ChildNodes.Count == 2) {
                parseNode.AstNode = new ArrayAccess { identifier = id, index = parseNode.ChildNodes[1].AstNode as Expression };
            } else {
                parseNode.AstNode = new Variable { identifier = id };
            }
            (parseNode.AstNode as Variable).span = Span.Of(parseNode);
        }

        public static void Condition(AstContext context, ParseTreeNode parseNode) {
            if (parseNode.ChildNodes.Count == 3) {
                var childType = parseNode.ChildNodes[0].Term.Name;
                if (childType == "Expression") {
                    var op = MatchComparisonOperator(parseNode.ChildNodes[1].Token.Text);
                    op.left = parseNode.ChildNodes[0].AstNode as Expression;
                    op.right = parseNode.ChildNodes[2].AstNode as Expression;
                    parseNode.AstNode = op;
                } else if (childType == "Condition") {
                    var op = MatchBooleanOperator(parseNode.ChildNodes[1].Token.Text);
                    op.left = parseNode.ChildNodes[0].AstNode as Condition;
                    op.right = parseNode.ChildNodes[2].AstNode as Condition;
                    parseNode.AstNode = op;
                }
            } else if (parseNode.ChildNodes.Count == 2 && parseNode.ChildNodes[0].Token.Text == "!") {
                parseNode.AstNode = new NOT { condition = parseNode.ChildNodes[1].AstNode as Condition };
            } else {
                parseNode.AstNode = parseNode.ChildNodes[0].AstNode as Condition;
            }
            (parseNode.AstNode as Condition).span = Span.Of(parseNode);
        }

        static Comparison MatchComparisonOperator(string op) {
            switch (op) {
                case "<": return new LT();
                case "<=": return new LEQ();
                case ">": return new GT();
                case ">=": return new GEQ();
                case "==": return new EQ();
                case "!=": return new NEQ();
                default: throw new ArgumentException("Unknown comparison operator: " + op);
            }
        }

        static BooleanOperator MatchBooleanOperator(string op) {
            switch (op) {
                case "&&": return new AND();
                case "||": return new OR();
                default: throw new ArgumentException("Unknown boolean operator: " + op);
            }
        }

        public static void Identifier(AstContext context, ParseTreeNode parseNode) {
            parseNode.AstNode = new Identifier { name = parseNode.Token.Text, span = Span.Of(parseNode) };
        }
    }
}
