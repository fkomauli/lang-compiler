﻿using System;
using System.IO;
using Lang;

class Compiler {

    static void Main(string[] args) {
        if (args.Length == 0) { PrintUsage(); return; }
        var optimize = false;
        foreach (var argument in args) {
            if (argument == "-O") { optimize = true; continue; }
            if (!File.Exists(argument)) { Console.WriteLine("Cannot find file " + argument); return; }
            var source = File.ReadAllText(argument);
            var name = argument.EndsWith(".lang") ? argument.Substring(0, argument.Length - 5) : argument;
            LangCompiler.Compile(name, source, optimize);
        }
    }

    static void PrintUsage() {
        Console.WriteLine("Usage: lc [-O] [filename...]");
        Console.WriteLine();
        Console.WriteLine("Options:");
        Console.WriteLine("    -O   enable optimizations");
    }
}
